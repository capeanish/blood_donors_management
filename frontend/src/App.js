import React, { Component } from "react";

import "./App.css";
import Routers from "./Routers";
import "../src/styles/style.css";
import "../src/styles/donorForm.css";


class App extends Component {
    constructor(props) {
        super(props);
        this.loginHandler = this.loginHandler.bind(this);
    }

    state = {
        loggedIn: false,
    };

    componentDidMount() {
        if (sessionStorage.getItem('loggedUser')) {
            this.setState((prevState) => ({
                loggedIn: !prevState.loggedIn,
            }));
        }
    }

    loginHandler = () => {
        if (this.state.loggedIn) {
            sessionStorage.clear();
            window.location.href = "/";
        }
        this.setState((prevState) => ({
            loggedIn: !prevState.loggedIn,
        }));
    };

    render() {
        return (<
            Routers loggedIn={this.state.loggedIn}
            loginHandler={this.loginHandler}
        />
        );
    }
}

export default App;