import React from "react";
import axios from "axios";
import BloodDonorItem from "./BloodDonorItem";
import { Link } from "react-router-dom";

export class BloodDonorsPage extends React.Component {

  state = {
    donors: [],
  };

  componentDidMount() {
    axios.get(`http://localhost:9000/api/donor/getall`).then((res) => {
      const donors = res.data;
      this.setState({ donors });
    });
  }

  goToCreateDonorPage() {

  }

  render() {
    return (
      <div>
        <div>
          <Link name="createDonor" id="createDonor" to={`/blood-donors/create`}>Create Donor</Link>
        </div>

 <ul className="bloodDonorList">
        {this.state.donors.map((donor) => (
          <BloodDonorItem key={donor.id} donor={donor} />
        ))}
      </ul>
      </div>

    );
  }
}
