import React from 'react';
import "../styles/donorForm.css";

export default class DonorFormPage extends React.Component {
    render() {
        return (
            <div className="container">
                <form action="">
                    <label htmlFor="name">Name</label>
                    <input type="text" id="name" name="name" placeholder="Your name.." />

                    <label htmlFor="dob">Date of Birth</label>
                    <input type="text" id="dob" name="dob" placeholder="Your Date of Birth.." />

                    <label htmlFor="email">Email</label>
                    <input type="text" id="email" name="email" placeholder="Your Email.." />

                    <label htmlFor="contactno">Contact No.</label>
                    <input type="text" id="contactno" name="contactno" placeholder="Your Contact Number.." />

                    <label htmlFor="altcontactno">AlternateContact No.</label>
                    <input type="text" id="altcontactno" name="altcontactno" placeholder="Your Contact Number.." />

                    <label htmlFor="bloodgroup">Blood Group</label>
                    <select id="bloodgroup" name="bloodgroup">
                        <option value="A+">A+</option>
                        <option value="B+">B+</option>
                        <option value="C-">A-</option>
                    </select>
                    <label htmlFor="subject">Subject</label>
                    <textarea id="subject" name="subject" placeholder="Write something.."></textarea>
                    <input type="submit" value="Submit" />
                </form>
            </div>
        );
    }
}
