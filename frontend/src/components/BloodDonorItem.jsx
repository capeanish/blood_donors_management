import React from "react";
import axios from "axios";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faPenSquare, faTrashAlt } from '@fortawesome/free-solid-svg-icons'

export default class BloodDonorItem extends React.Component {
  state = {
    donor: {},
  };

  componentDidMount() {
    axios
      .get(`http://localhost:9000/api/donor/get/` + this.props.donor.id)
      .then((res) => {
        const donor = res.data;
        this.setState({ donor });
      });
  }

  convertTimeStampToDate() {
    const timestamp = Date.now();
    return (new Intl.DateTimeFormat('en-US', { year: 'numeric', month: '2-digit', day: '2-digit' }).format(timestamp));
  }

  _calculateAge(birthday) {
    var ageDifMs = Date.now() - birthday;
    var ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }

  render() {
    return <li>
      <div className='donorContainer'>
        <div className="donorContainerLeft">
          <b>{this.state.donor.name}&nbsp;<span className="bloodGroupLabel">(&nbsp;Blood Group:&nbsp;{this.state.donor.blood_group}&nbsp;)</span></b><br />
          <span>Last Donated On: {this.convertTimeStampToDate(this.state.donor.last_donated_date)}</span><br />
          <span>Age:&nbsp;{this._calculateAge(this.state.donor.date_of_birth)}&nbsp;years</span>
        </div>
        <div className="donorContainerRight">
          <FontAwesomeIcon icon={faEye} size="lg" />&nbsp;&nbsp;
          <FontAwesomeIcon icon={faPenSquare} size="lg" />&nbsp;&nbsp;
          <FontAwesomeIcon icon={faTrashAlt} size="lg" />
        </div>
      </div>
    </li>;
  }
}
