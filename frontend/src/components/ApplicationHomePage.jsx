import React from "react";
import { Redirect } from "react-router-dom";

export default class ApplicationHomePage extends React.Component {
  state = {
    user: {}
  };

  componentDidMount() {
    const user = JSON.parse(sessionStorage.getItem('loggedUser')).user;
    this.setState({ user });
  }

  render() {
    if (this.props.loggedIn) {
      return (
        <div>
          <h2>Welcome {this.state.user.first_name} {this.state.user.last_name}!</h2>
        </div>
      );
    } else {
      return <Redirect to="/login" />;
    }
  }
}
