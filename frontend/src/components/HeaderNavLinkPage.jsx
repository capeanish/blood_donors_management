import React from "react";
import { NavLink } from "react-router-dom";

export default class HeaderNavLinkPage extends React.Component {
  render() {
    if (this.props.loggedIn) {
      return (
        <ul className='headerList'>
          <li>
            <NavLink to="/home" activeStyle={{ color: "green" }}>
              Appilcation home
            </NavLink>
          </li>
          <li>
            <NavLink to="/blood-donors" exact activeStyle={{ color: "green" }}>
              Blood Donors
            </NavLink>
          </li>
          <li>
            <span className="logOutLink" onClick={this.props.loginHandler}>Logout</span>
          </li>
        </ul>
      );
    } else {
      return (
        <ul className='headerList'>
          <li>
            <NavLink to="/" exact activeStyle={{ color: "green" }}>
              Home
            </NavLink>
          </li>
          <li>
            <NavLink to="/about" exact activeStyle={{ color: "green" }}>
              About
            </NavLink>
          </li>
          <li>
            <NavLink to="/login" exact className="logInLink">
              Login
            </NavLink>
          </li>
        </ul>
      );
    }
  }
}
