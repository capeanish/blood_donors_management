import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Redirect } from "react-router";
import { Route } from "react-router-dom";
import ApplicationHomePage from "./components/ApplicationHomePage";
import AboutPage from "./components/AboutPage";
import HeaderNavLinkPage from "./components/HeaderNavLinkPage";
import HomePage from "./components/HomePage";
import LoginPage from "./components/LoginPage";
import { BloodDonorsPage } from "./components/BloodDonorsPage";
import DonorFormPage from "./components/DonorFormPage";

export default class Routers extends React.Component {
  render() {
    return (
      <Router>
        <Redirect push to="/" />
        <div>
          <HeaderNavLinkPage loggedIn={this.props.loggedIn} loginHandler={this.props.loginHandler} />
          <Route
            exact
            path="/"
            render={() => {
              return <HomePage />;
            }}
          />
          <Route
            exact
            strict
            path="/about"
            render={() => {
              return <AboutPage />;
            }}
          />
          <Route
            exact
            path="/login"
            render={() => {
              return <LoginPage loggedIn={this.props.loggedIn} loginHandler={this.props.loginHandler} />;
            }}
          />
          <Route
            exact
            path="/blood-donors"
            render={() => {
              return <BloodDonorsPage />;
            }}
          />
          <Route
            exact
            path="/blood-donors/create"
            render={() => {
              return <DonorFormPage />;
            }}
          />
          <Route
            exact
            strict
            path="/home"
            render={({ match }) => {
              return (
                <ApplicationHomePage
                  loginHandler={this.props.loginHandler}
                  loggedIn={this.props.loggedIn}
                />
              );
            }}
          />
        </div>
      </Router>
    );
  }
}
