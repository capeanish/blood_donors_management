Name: Blood Donors Management API

Used technology: 
1.	NodeJS (version 12.18.0)
2.	PostgreSQL (version 13)

Database name: blood
Table name: donor

API List:
1.	CREATE
2.	GET ALL
3.	GET BY ID
4.	UPDATE BY ID
5.	DELETE BY ID
6.	IS DONOR ALREADY EXISTS

CREATE:
http://localhost:9000/api/donor/create
Method:  POST
Sample Request Body
--------------------------------------
{
  "name": "Anish Kumar",
  "date_of_birth": 564675060,
  "email": "anish@gmail.com",
  "contact_no": "9876543210",
  "alternate_contact_no": "9876542310",
  "blood_group": "A1-",
  "last_donated_date": 1560988800,
  "stopped_donating": false
}
--------------------------------------


GET ALL
http://localhost:9000/api/donor/getall
Method: GET


GET BY ID
http://localhost:9000/api/donor/get/1
Method: GET

UPDATE BY ID
http://localhost:9000/api/donor/update/1
Method:  PUT
Sample Request Body
--------------------------------------
{
  "stopped_donating": true
}
--------------------------------------

DELETE BY ID
http://localhost:9000/api/donor/delete/1
Method:  DELETE

IS DONOR ALREADY EXISTS
http://localhost:9000/api/donor/exists
Method:  POST
Sample Request Body
--------------------------------------
{
  "email": "anish@gmail.com"
}
--------------------------------------






