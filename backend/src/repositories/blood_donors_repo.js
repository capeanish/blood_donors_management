const psql_client = require('../db/postgres');

module.exports.createDonor = (donor, callback) => {
    psql_client
        .insert(donor)
        .into("donors")
        .returning("*")
        .then(rows => callback(null, rows))
        .catch(error => callback(error, null));
}

module.exports.updateDonor = (id, donor, callback) => {
    psql_client
        .from('donors')
        .where('id', '=', id)
        .update(donor)
        .then(() => callback(null, `Donor with id->${id} updated successfully`))
        .catch(error => callback(error, null));
}

module.exports.getAllDonors = (callback) => {
    const query = "SELECT * FROM public.donors";
    psql_client
        .raw(query)
        .then(rows => callback(null, rows))
        .catch(error => callback(error, null));
}

module.exports.getDonorById = (donorId, callback) => {
    const query = `SELECT * FROM public.donors WHERE id=${donorId}`;
    psql_client
        .raw(query)
        .then(rows => callback(null, rows))
        .catch(error => callback(error, null));
}

module.exports.deleteDonorById = (id, callback) => {
    psql_client
        .from('donors')
        .where({ id })
        .del()
        .then(() => callback(null, `Donor with id->${id} deleted successfully`))
        .catch(error => callback(error, null));
}

module.exports.isDonorAlreadyExists = (donor, callback) => {
    const query = `SELECT * FROM public.donors WHERE email='${donor.email}'`;
    psql_client
        .raw(query)
        .then(rows => callback(null, rows))
        .catch(error => callback(error, null));
}

