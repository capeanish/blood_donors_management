var usersController = require('../controllers/users_controller');

module.exports = function(router) {
    router
        .route('/api/user/create')
        .post(usersController.createUser);
    router
        .route('/api/user/update/:id')
        .put(usersController.updateUser);
    router
        .route('/api/user/delete/:id')
        .delete(usersController.deleteUserById);
    router
        .route('/api/user/getall')
        .get(usersController.getAllUsers);
    router
        .route('/api/user/get/:id')
        .get(usersController.getUserById);
    router
        .route('/api/user/login')
        .post(usersController.login);
    return router;
}