const constants = {
    user: {
        DEFAULT_SUPER_ADMIN_USER_ID: 1,
    },
    http_status: {
        OK: 200,
        BAD_REQUEST: 400,
        UNAUTHORIZED: 401,
        FORBIDDEN: 403,
        NOT_FOUND: 404,
        INTERNAL_SERVER_ERROR: 500,
        BAD_GATEWAY: 502,
        SERVICE_UNAVAILABLE: 503,
        GATEWAY_TIMEOUT: 504,
    },
    SECRET_KEY: "2504166E48DC19294B86773F798DEE7996D3973E",
    TOKEN_VALIDITY_SEVEN_DAYS: 7,
};

module.exports = constants;